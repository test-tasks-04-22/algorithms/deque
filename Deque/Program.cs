﻿using Deque;

var deque = new Deque<string>();
deque.AddFirst("1");
deque.AddLast("2");
deque.AddLast("3");
deque.AddLast("4");
             
Console.Write("Дек после добавления элементов: ");
foreach (var item in deque)
    Console.Write($"{item} ");
             
var firstRemoved = deque.RemoveFirst();
Console.WriteLine($"\nУдалённый первый элемент: {firstRemoved}");
var lastRemoved = deque.RemoveLast();
Console.WriteLine($"Удалённый последний элемент: {lastRemoved}");
Console.WriteLine($"Первый элемент изменённого дека: {deque.First}");
Console.WriteLine($"Последний элемент дека: {deque.Last}");

Console.Write("Дек после изменения: ");
foreach (var item in deque)
    Console.Write($"{item} ");
Console.WriteLine($", {deque.Count()} элементов");

var isPresent = deque.Contains("5");
Console.WriteLine(isPresent ? "5 присутствует" : "5 отсутствует");

deque.Clear();

Console.WriteLine(!deque.Any() ? "Элементы дека удалены" : "Дек не пуст"); 