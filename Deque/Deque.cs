﻿using System.Collections;
using System.Diagnostics;

namespace Deque;

public class DoublyNode<T>
{
    public DoublyNode(T data)
    {
        Data = data;
    }
    public T Data { get; }
    public DoublyNode<T>? Previous { get; set; }
    public DoublyNode<T>? Next { get; set; }
}

public class Deque<T> : IEnumerable<T>
{
    private DoublyNode<T>? _head;
    private DoublyNode<T>? _tail;

    public void AddLast(T data)
    {
        var node = new DoublyNode<T>(data);

        if (_head == null)
            _head = node;
        else
        {
            Debug.Assert(_tail != null, nameof(_tail) + " != null");
            _tail.Next = node;
            node.Previous = _tail;
        }
        _tail = node;
        Count++;
    }
    public void AddFirst(T data)
    {
        var node = new DoublyNode<T>(data);
        var temp = _head;
        node.Next = temp;
        _head = node;
        if (Count == 0)
            _tail = _head;
        else
        {
            Debug.Assert(temp != null, nameof(temp) + " != null");
            temp.Previous = node;
        }

        Count++;
    }
    public T RemoveFirst()
    {
        if (Count == 0)
            throw new InvalidOperationException();
        Debug.Assert(_head != null, nameof(_head) + " != null");
        T output = _head.Data;
        if(Count==1)
        {
            _head = _tail = null;
        }
        else
        {
            _head = _head.Next;
            Debug.Assert(_head != null, nameof(_head) + " != null");
            _head.Previous = null;
        }
        Count--;
        return output;
    }
    public T RemoveLast()
    {
        if (Count == 0)
            throw new InvalidOperationException();
        Debug.Assert(_tail != null, nameof(_tail) + " != null");
        T output = _tail.Data;
        if (Count == 1)
        {
            _head = _tail = null;
        }
        else
        {
            _tail = _tail.Previous;
            Debug.Assert(_tail != null, nameof(_tail) + " != null");
            _tail.Next = null;
        }
        Count--;
        return output;
    }
    public T First
    {
        get
        {
            if (IsEmpty)
                throw new InvalidOperationException();
            Debug.Assert(_head != null, nameof(_head) + " != null");
            return _head.Data;
        }
    }
    public T Last
    {
        get
        {
            if (IsEmpty)
                throw new InvalidOperationException();
            Debug.Assert(_tail != null, nameof(_tail) + " != null");
            return _tail.Data;
        }
    }

    private int Count { get; set; }
    private bool IsEmpty => Count == 0;

    public void Clear()
    {
        _head = null;
        _tail = null;
        Count = 0;
    }

    public bool Contains(T data)
    {
        var current = _head;
        while (current != null)
        {
            Debug.Assert(current.Data != null, "current.Data != null");
            if (current.Data.Equals(data))
                return true;
            current = current.Next;
        }
        return false;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)this).GetEnumerator();
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        var current = _head;
        while (current != null)
        {
            yield return current.Data;
            current = current.Next;
        }
    }
}
